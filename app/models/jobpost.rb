class Jobpost < ApplicationRecord
  belongs_to :User
  has_many :applications, dependent: :destroy, foreign_key: :jobpost_id
  validates_presence_of :title,:description

end
