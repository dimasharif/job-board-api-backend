class User < ApplicationRecord
	  has_secure_password
	  has_many :jobposts, dependent: :destroy, foreign_key: :User_id
	  has_many :applications, dependent: :destroy , foreign_key: :User_id
	  validates_presence_of :email,:password_digest,:name
end
