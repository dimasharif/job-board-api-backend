class JobpostsController < ApplicationController
before_action :set_jobpost, only: [:show, :update, :destroy]

  def index
    @jobposts = Jobpost.all
    json_response(@jobposts)
  end


  def show
    json_response(@jobpost)
  end

  def create
    if current_user.isseeker === false 
      @jobpost = current_user.jobposts.create!(jobpost_params)
      json_response(@jobpost, :created)
    else
      render json:{message: "Validation failed"} , status: :unprocessable_entity
    end
  end

  def update
    if current_user.isseeker === false
    	@jobpost = current_user.jobposts
      @jobpost.update(jobpost_params)
    else
      render json:{message: "Validation failed"} , status: :unprocessable_entity
    end
    head :no_content
  end

  def destroy
    if current_user.isseeker === false
      @jobpost = current_user.jobposts.find_by(id: params[:id])
      @jobpost.destroy  
      json_response("successfully deleted")  
    else
      render json:{message: "Validation failed"} , status: :unprocessable_entity
    end
    head :no_content
  end

  private

  def jobpost_params
    params.permit(:title,:description)
  end

  def set_jobpost
    @jobpost = Jobpost.find(params[:id])
  end
end
