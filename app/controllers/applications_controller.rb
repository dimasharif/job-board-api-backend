class ApplicationsController < ApplicationController
  before_action :set_user

def index
  @jobpost = Jobpost.find_by(id: params[:jobpost_id] , User_id: @user.id)
    if @jobpost.present? && @user.isseeker === false
        @application = Application.select { |app| app.jobpost_id===@jobpost.id }
        if @application.present?
          json_response(@application)
          else
            json_response("Couldn't find Application")
          end
    else json_response("Validation error, post doesn't exist or you're not a recruiter.")
          end
    
def show
  @jobpost = Jobpost.find_by(id: params[:jobpost_id])
    if @jobpost.present? && @user.isseeker === false
        @application = Application.find_by(jobpost_id: params[:jobpost_id], id: params[:id])
        if @application.present?
          @user_id = @application.User_id
          @userInfo = User.find_by(id: @user_id)
          @application.appstatus = true
          @application.save
           render json:{email:@userInfo.email, name:@userInfo.name, applicationstatus: @application.appstatus} , status: :ok
          else
           render json:{message: "Couldn't find Application"} , status: :not_found
          end
      else json_response("Validation error, post doesn't exist or you're not a recruiter.")
          end
     end


def create
  @jobpost = Jobpost.find_by(id: params[:jobpost_id])
    if @jobpost.present? && @user.isseeker === true          
      @application = current_user.applications.create!(User_id:@user.id,jobpost_id:params[:jobpost_id],appstatus:false)
        if @application.present?
              json_response(@application)
          else
            render json:{message: "Validation failed: Appstatus can't be blank"} , status: :unprocessable_entity
          end
          else
               render json:{message: "Couldn't create application, you're a recruiter."}
          end  
     end
 end






 private
  def set_user
   @user = current_user
  end
