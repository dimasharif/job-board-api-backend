require 'rails_helper'
require 'pp'
RSpec.describe 'Jobposts', type: :request do
  let(:user) { create(:user) }
  let!(:jobposts) { create_list(:jobpost, 10, User_id: user.id) }
  let(:jobpost_id) { jobposts.first.id }
  let(:headers) { valid_headers }

  describe 'GET /jobposts' do
    before { get '/jobposts', params: {}, headers: headers }

    it 'returns jobposts' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /jobposts/:id' do
    before { get "/jobposts/#{jobpost_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the jobpost' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(jobpost_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:jobpost_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Jobpost/)
      end
    end
  end

  describe 'POST /jobposts' do
     let(:valid_attributes) do
      { title: 'Learn Elm', description: 'hi',user_id: user.id.to_s }.to_json
    end

    context 'when request is valid' do
      before { post '/jobposts', params: valid_attributes, headers: headers }

      it 'creates a jobpost' do
        expect(json['title']).to eq('Learn Elm')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      let(:invalid_attributes) { { title: nil,description:nil }.to_json }
      before { post '/jobposts', params: invalid_attributes, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(json['message'])
          .to match(/Validation failed: Title can't be blank, Description can't be blank/)
      end

      end
    end
  describe 'PUT /jobposts/:id' do
    let(:valid_attributes) { { title: 'Shopping',description:'hiiiii' }.to_json }

    context 'when the record exists' do
      before { put "/jobposts/#{jobpost_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
        end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /jobposts/:id' do
    before { delete "/jobposts/#{jobpost_id}", params: {valid_attributes}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end