require 'rails_helper'
require 'pp'
RSpec.describe 'Applications', type: :request do
  let(:user) { create(:user) }
  let!(:jobpost) { create(:jobpost,User_id: user.id) }
  let!(:applications) { create_list(:application, 3, jobpost_id: jobpost.id,User_id:user.id) }
  let(:jobpost_id) {jobpost.id}
  let(:id) { applications.first.id }
  let(:headers) { valid_headers }

  describe 'GET /jobposts/:jobpost_id/applications' do
    before { get "/jobposts/#{jobpost_id}/applications", params: {}, headers: headers }

    it 'returns applications' do
      expect(json).not_to be_empty
      expect(json.size).to eq(3)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
end
    describe 'GET /jobposts/:jobpost_id/applications/:id' do
    before { get "/jobposts/#{jobpost_id}/applications/#{id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the application' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(nil)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Application/)
      end
    end
  end


  describe 'POST /jobposts/:jobpost_id/applications' do
     let(:valid_attributes) do
      { appstatus:false,User_id: user.id.to_s, jobpost_id: jobpost_id.to_s  }.to_json
    end

    context 'when request is valid' do
      before { post "/jobposts/#{jobpost_id}/applications", params: valid_attributes, headers: headers }

      it 'creates an application' do
        expect(json['appstatus']).to eq(nil)
      end

      end

    end
end