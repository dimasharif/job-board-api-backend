require 'rails_helper'

RSpec.describe User, type: :model do
it { should have_many(:jobposts).dependent(:destroy) }
it { should have_many(:applications).dependent(:destroy) }
it { should validate_presence_of(:email) }
it { should validate_presence_of(:password_digest) }
it { should validate_presence_of(:name) }
end

