require 'rails_helper'

RSpec.describe Jobpost, type: :model do
  it { should have_many(:applications).dependent(:destroy) }
  it { should belong_to(:User) }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
end