FactoryBot.define do
  factory :jobpost do
    title { Faker::Lorem.word }
    description { Faker::Lorem.word }
    User_id {nil}
  end
end