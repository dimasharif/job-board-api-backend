class CreateApplications < ActiveRecord::Migration[6.1]
  def change
    create_table :applications do |t|
      t.boolean :appstatus
      t.references :User, null: false, foreign_key: true
      t.references :jobpost, null: false, foreign_key: true

      t.timestamps
    end
  end
end
