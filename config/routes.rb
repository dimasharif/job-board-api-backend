Rails.application.routes.draw do
  resources :jobposts do
  resources :applications
  end
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'

end
