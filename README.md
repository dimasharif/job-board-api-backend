# Backend Job Board Documentation

# To sign up in the application either as a job seeker or a job recruiter
Done by entering the email, name and password in addition to a true value if the user is seeking a job, and no if they are recruiting for a job.
POST /signup?email={email}&name={name}&password={password}&password_confirmation={re-enter the password for authentication purposes}&isseeker={true/false}

# To sign in to the application
Done by entering the email and password.
POST /auth/login?email={email}&password={password}

# Further Actions

# 1. For recruiters (after logging in)


# To create a job post 
Done by entering the job title and description.
POST /jobposts?title={title}&description={description}


# To list all job posts
GET /jobposts/


# To get a job post; using a specific ID
Done by entering the job ID.
GET /jobposts/:jobpost_id


# To update a specific job post that is created by them
Done by entering the job ID, and entering the attribute name/s that the recruiter seeks to change
PUT /jobposts/:jobpost_id?attributename={attributevalue}&..(optional)


# To delete a specific job post that is created by them
Done by entering the job ID.
DELETE /jobposts/:jobpost_id/


# To list all applications for a specific job post that is created by them
Done by entering the job ID, routing to its applications.
GET /jobposts/:jobpost_id/applications/


# To get a specific application for a specific job post that is created by them
Done by entering the job ID, routing to its applications; and finally to that application by entering its ID.
GET /jobposts/:jobpost_id/applications/:application_id



# 2. For job seekers (after logging in)


# To list all job posts
GET /jobposts/


# To list job post; choosing a specific ID
Done by entering the job ID.
GET /jobposts/:jobpost_id


# To create a job application
Done by entering the intended job id.
POST /jobposts/:jobpost_id/applications?




